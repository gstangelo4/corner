﻿/*
Greg St. Angelo IV
1.22.2018
*/
using UnityEngine;

public class ApplicationInstance : MonoBehaviour
{
    public static ApplicationInstance Application;

    private void Awake()
    {
        if(Application == null)
        {
            Application = this;
        }
        else if(Application != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
